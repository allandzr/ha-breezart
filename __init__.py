"""The breezart 550 lux integration."""
from __future__ import annotations
import asyncio

from homeassistant.config_entries import ConfigEntry
from homeassistant.core import HomeAssistant
from homeassistant.helpers.typing import ConfigType
import asyncio
import logging
import requests

from .const import DOMAIN

# TODO List the platforms that you want to support.
# For your initial PR, limit it to 1 platform.
PLATFORMS: list[str] = ["climate"]
_LOGGER = logging.getLogger(__name__)


async def async_setup(hass: HomeAssistant, config: ConfigType) -> bool:
    """Set up the Hello World component."""
    # Ensure our name space for storing objects is a known type. A dict is
    # common/preferred as it allows a separate instance of your class for each
    # instance that has been created in the UI.
    hass.data.setdefault(DOMAIN, {})
    return True


async def async_setup_entry(hass: HomeAssistant, entry: ConfigEntry) -> bool:
    """Set up breezart 550 lux from a config entry."""
    # TODO Store an API object for your platforms to access
    # hass.data[DOMAIN][entry.entry_id] = MyApi(...)

    entry.async_on_unload(entry.add_update_listener(_async_update_listener))

    hass.data[DOMAIN][entry.entry_id] = Breezart(hass, entry)
    hass.config_entries.async_setup_platforms(entry, PLATFORMS)

    return True


async def _async_update_listener(hass: HomeAssistant, entry: ConfigEntry) -> None:
    """Handle options update."""
    await hass.config_entries.async_reload(entry.entry_id)


async def async_unload_entry(hass: HomeAssistant, entry: ConfigEntry) -> bool:
    """Unload a config entry."""
    unload_ok = await hass.config_entries.async_unload_platforms(entry, PLATFORMS)
    if unload_ok:
        hass.data[DOMAIN].pop(entry.entry_id)

    return unload_ok


class BreezartStatus:
    power_state = None
    speed = None
    temp = None

    def __init__(self, data=None) -> None:
        if data:
            raw_speed = data[7]
            if raw_speed < 10:
                speed = 0
            else:
                speed = (raw_speed - 1) / 1000

            self.power_state = data[0]
            self.speed = speed
            self.temp = round(data[3] / 10, 1)

        _LOGGER.debug(
            f"New status power: {self.power_state} speed: {self.speed} temp: {self.temp}"
        )

    def is_power_on(self):
        return True if self.power_state == 1 else False

    def current_temp(self):
        return self.temp or 0


class Breezart:
    status = BreezartStatus()
    new_status = BreezartStatus()

    def __init__(self, hass: HomeAssistant, entry: ConfigEntry) -> None:
        self._hass = hass
        self._entry = entry
        self._host = entry.data["host"]
        self.modbus = BreezartModbus(hass)
        _LOGGER.info(f"Init breezart {self._host}")

    async def fetch_data(self):
        _LOGGER.debug("Fetch data called")
        await self.update_status()

    async def test_connection(self) -> bool:
        _LOGGER.info(f"test connection breezarf {self._host}")
        """Test connectivity to the Dummy hub is OK."""
        await asyncio.sleep(1)
        return True

    def has_status(self):
        return self.status

    def get_power_status(self):
        return self.status.get("power_state")

    async def async_power_off(self):
        await self.modbus.async_power_off(self._host)

    async def async_power_on(self):
        await self.modbus.async_power_on(self._host)

    async def update_status(self):
        self.status = await self.modbus.async_get_status(self._host)


class BreezartModbus:
    def __init__(self, hass: HomeAssistant) -> None:
        self.hass = hass
        self.port = 80
        self.CntRequest = 0
        self.ReadHR = 3
        self.ReadIR = 4
        self.WriteSHR = 6
        self.WriteMHR = 16

        self.CntRequest = 0

    async def async_power_off(self, host):
        return await self.send_request_async(host, self.WriteSHR, 3, 1)

    async def async_power_on(self, host):
        return await self.send_request_async(host, self.WriteSHR, 3, 1, [1])

    async def async_get_status(self, host):
        data = await self.send_request_async(host, self.ReadIR, 10, 13)
        return BreezartStatus(data)

    async def send_request_async(self, sIP, TypeReqv, MBAddr, NReg, RegData=[0]):
        _LOGGER.debug(
            f"Send requert to {sIP} type {TypeReqv} MBAddr {MBAddr} data {RegData}"
        )
        addr = "http://" + str(sIP)
        timeout = 3
        LenBuff = 12

        self.CntRequest += 1
        SesID = (self.CntRequest + 0x10000) % 0x10000

        _LOGGER.debug(f"SesID {SesID}")

        data = bytearray(LenBuff)
        data[0] = (SesID >> 8) & 0xFF
        data[1] = SesID & 0xFF
        data[2] = 0
        data[3] = 0
        data[4] = 0
        if TypeReqv == self.WriteMHR:
            data[5] = 7 + NReg * 2
        else:
            data[5] = 6
        data[5] = 6
        data[6] = 1
        data[7] = TypeReqv
        data[8] = (MBAddr >> 8) & 0xFF
        data[9] = MBAddr & 0xFF
        if TypeReqv == self.ReadHR or TypeReqv == self.ReadIR:
            data[10] = 0
            data[11] = NReg
        elif TypeReqv == self.WriteSHR:
            data[10] = (RegData[0] >> 8) & 0xFF
            data[11] = RegData[0] & 0xFF
        elif TypeReqv == self.WriteMHR:
            data[10] = 0
            data[11] = NReg
            data[12] = NReg * 2
            for i in range(NReg - 1):
                data[i * 2 + 13] = (RegData[i] >> 8) & 0xFF
                data[i * 2 + 14] = RegData[i] & 0xFF

        resp = await self.hass.async_add_executor_job(
            lambda: requests.post(addr, data, timeout=timeout)
        )

        _LOGGER.debug("Response status " + str(resp.status_code))

        respData = resp.content
        RespSesID = (respData[0] << 8) | respData[1]
        if SesID != RespSesID:
            _LOGGER.debug(f"Wrong ses id {RespSesID}")
        if respData[7] > 0x80:
            _LOGGER.debug("Modbus Error #" + str(respData[8]))

        MBReg = []

        for i in range(int(respData[8] / 2) - 1):
            MBReg.append(respData[i * 2 + 9] << 8 or respData[i * 2 + 10])

        return MBReg
