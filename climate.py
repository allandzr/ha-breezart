from homeassistant.components.breezart import Breezart
from .const import DOMAIN, UPDATE_INTERVAL
from homeassistant.components.climate import ClimateEntity
import logging
import uuid
import async_timeout
from homeassistant.helpers.update_coordinator import (
    CoordinatorEntity,
    DataUpdateCoordinator,
    UpdateFailed,
)
from homeassistant.exceptions import ConfigEntryAuthFailed
from datetime import timedelta


from homeassistant.components.climate.const import (
    CURRENT_HVAC_FAN,
    CURRENT_HVAC_IDLE,
    CURRENT_HVAC_OFF,
    HVAC_MODE_FAN_ONLY,
    HVAC_MODE_HEAT,
    SUPPORT_FAN_MODE,
    SUPPORT_TARGET_TEMPERATURE,
    HVAC_MODE_OFF,
    FAN_OFF,
    FAN_AUTO,
    ATTR_HVAC_MODE,
)

from homeassistant.const import (
    ATTR_TEMPERATURE,
    PRECISION_WHOLE,
    TEMP_CELSIUS,
    STATE_UNKNOWN,
)


_LOGGER = logging.getLogger(__name__)


async def async_setup_entry(hass, config_entry, async_add_entities):
    """Add sensors for passed config_entry in HA."""

    api: Breezart = hass.data[DOMAIN][config_entry.entry_id]

    async def async_update_data():
        # try:
        # Note: asyncio.TimeoutError and aiohttp.ClientError are already
        # handled by the data update coordinator.
        async with async_timeout.timeout(10):
            return await api.fetch_data()

        # except ApiError as err:
        #     raise UpdateFailed(f"Error communicating with API: {err}")

    coordinator = DataUpdateCoordinator(
        hass,
        _LOGGER,
        # Name of the data. For logging purposes.
        name="Breezart state",
        update_method=async_update_data,
        update_interval=timedelta(seconds=UPDATE_INTERVAL),
    )

    await coordinator.async_config_entry_first_refresh()

    devices = []
    devices.append(Breezart550LUX(coordinator, api))
    async_add_entities(devices)


class Breezart550LUX(CoordinatorEntity, ClimateEntity):
    _api: Breezart

    def __init__(self, coordinator, api: Breezart) -> None:
        super().__init__(coordinator)
        self._api: Breezart = api

    async def async_update(self):
        pass

    @property
    def should_poll(self) -> bool:
        return True

    @property
    def turn_on(self):
        pass

    @property
    def turn_off(self):
        pass

    @property
    def available(self) -> bool:
        """Return True if entity is available."""
        return True

    @property
    def unique_id(self):
        id = uuid.uuid1().fields[-1]
        _LOGGER.info(f"get id {id}")
        """Return a unique id identifying the entity."""
        return id

    @property
    def name(self):
        return "Breezart 550 LUX"

    @property
    def fan_modes(self):
        _fan_modes = [FAN_OFF, FAN_AUTO]
        return _fan_modes

    def set_fan_mode(self, fan_mode: str) -> None:
        _LOGGER.info(f"Set fan mode to {fan_mode}")
        return True

    @property
    def hvac_mode(self):
        """Return current operation ie. heat, cool, idle."""
        return HVAC_MODE_FAN_ONLY if self._api.status.is_power_on() else HVAC_MODE_OFF

    @property
    def fan_mode(self):
        return FAN_AUTO if self._api.status.is_power_on() else FAN_OFF

    @property
    def hvac_modes(self):
        """Return the list of available operation modes."""
        _operations = [HVAC_MODE_FAN_ONLY, HVAC_MODE_HEAT, HVAC_MODE_OFF]
        return _operations

    async def async_set_hvac_mode(self, hvac_mode: str) -> None:
        _LOGGER.info(f"Set HVAC MODE {hvac_mode}")
        if hvac_mode == HVAC_MODE_OFF:
            await self._api.async_power_off()
        else:
            await self._api.async_power_on()

    @property
    def current_temperature(self):
        return self._api.status.current_temp()

    @property
    def speed(self) -> str:
        """Return the current speed."""
        return self._api.status.speed or STATE_UNKNOWN

    @property
    def target_temperature(self):
        """Return the temperature we try to reach."""
        return 0

    @property
    def temperature_unit(self):
        """Return the unit of measurement used by the platform."""
        return TEMP_CELSIUS

    @property
    def target_temperature_step(self):
        return 1

    @property
    def supported_features(self):
        """Return the list of supported features."""
        supports = SUPPORT_FAN_MODE
        supports |= SUPPORT_TARGET_TEMPERATURE
        return SUPPORT_FAN_MODE
